+++
title = 'Reality'
date = 2023-03-01T17:21:35-05:00
draft = false
+++

![](Storytellers-ZhaawanArt.jpg "Story Teller's Mirror")

[Image Source](https://www.zhaawanart.com/post/love-stories-from-the-land-of-many-lakes-part-15-wiinabozho-and-the-storytellers-mirror "Credit")

# Reality 

When I was younger I would often look on in awe when I witnessed someone effortlessly doing something that no matter how hard I tried, I simply could not do.

A time that sticks out in my memory, was one in which I was watching a friend sketching.
I watched as her hand flowed over the white page and was dumbfounded as I watched the process of the image unfold.

> "Hey, how in the world do you do that?"
>
> "Do what?"
> 
> "The image, your art, how does it just flow out onto the page like that?"
> 
> "I dunno, I just see the image in my head, and I do my best to put it on paper."
> 
> "Well, when you say you see the 'image' in your head? Do you mean that literally?"
> 
> "Well, yeah, I see the image in my mind, and then I try to get it out."

This moment stands out clearly to me because I realized at that time that other people could see images in their mind!
As surprised as I was by this, so she was that I could not see images in my mind.

With this I began to wonder how differently others minds must work.

It wouldn't be until 2015 before researchers would coin the term _aphantasia_. 
Like everything, it is a spectrum.
Some people have full video editing studios in their heads, and on the flip side, some people have only blackness.
Most of us, I suspect, fall somewhere in between the two ends of that spectrum.

I began wondering... _Well if our neural networks are programmed so differently, it really isn't a surprise that we would all have are own unique neural network._

One time I was attending the teacher's assistant session for a 100 level philosophy class on critical thinking.
Philosophy was easy for me.
My classmates were asking questions on very basic logic and I zoned out on something else...
After an hour or so, I had come out of my world, and was surprised to see that the class was still struggling with this basic logic.
I watched and listened, and the T.A.'s frustration was evident.

> I raised my hand and asked;
> 
> "Wait, they don't understand this?"
> 
> "Apparently not."
> 
> "Well, if they don't get this, how do they make decisions?"
> 
> "I don't know."
> 
> I looked around at the 60 or so students in attendance, wondering, how do all these people navigate decisions?

I have since come to understand there are other methods of decision making, that in fact a lot folks decide based on emotion.

I can see now, how pointless so many of my logical arguments were with so many.

---

Perception is unique to each observer.
Our grasp of reality is limited to our perceptions.

So, we all have our own unique reality.

We can easily see this play out everyday all around us.
One example could be take any event witnessed by some amount of people.
If you start interviewing those witnesses, you soon begin to get a lot of different stories.
All of those stories, stemmed from the same event, yet they are different.

Are all of those witnesses wrong, or are they all right, or is just one right?

What about that last argument I got in with my partner?
Is she right? 
Am I right?
I feel right, I think I'm right, I believe I am right...

But then I talk to her, and listen, and pretty soon, I am not so sure I am right. 
Can we both be right?
And both be wrong?

---

At the end of the day, I am really amazed that any of us manages to communicate at all.
We all have our own unique experiences, thoughts, actions, and deeds.
We all have our own unique ways of perceiving the world.

Ah but, what about _objective_ scientific measurements?

Just an extension of our perceptions.
Hence, with the new space telescopes, comes new perception, which brings a new reality.

And the scientific _community_ begins to reorganize their _ideas_ about reality.
This happens again and again over time as we perceive more as a species.

Just as we grow older, gain more experiences, and our realities change.
What was once important, is no longer.
What was once believed, is now forgotten.
Emotions, hurt, pain, love, joy, all of these change as we gain more experiences.

We humans are an interesting species.
We can not survive on our own, therefore our very survival depends on others.
This is obvious in traditional cultures, but we civilized folk, tend to forget this.

Traditional peoples _know_ their place in Creation, their survival depends upon it. 
And this isn't some civilized idea like those crazy "Reality TV" survival shows.
No, this is the ability to thrive in a viable culture. One that is healthy and strong within the _web of life_.

Many traditional peoples communicate their perceptions of reality in ... well, for lack of a better way to explain it to civilized people, is loosely _animism_.
Because we are so fond of words that easily explain and contain ideas, I'll just use the term here as a tool to communicate.

Mathematics are a tool to describe, communicate, and think about reality.

One morning I woke up and this question popped into my head;

"What came first? Mathematics or the Universe?"

As I pondered the question, I asked some coworkers what they thought the answer was, and the one that as an engineer, I found resonated the most with me was;

"The Universe. Mathematics is the documentation."

Puts a whole new spin on RTFM! :)

---

As civilized peoples, our perceptions of the natural world a.k.a. reality, has become so disconnected that the vast majority of us can not perceive that we have murdered the planet.
That our species, and 90% of all life on Earth, is now in hospice.
**Because** our people collectively, through our non-viable lifeways, have collectively committed murder suicide.
And we can't even admit it.

These words, have never rung truer to me than they do now.

> “I’ve witnessed one miracle after another through my whole life, extraordinary things happening. I see that God tries very hard and apparently is intent to make us a success if it is possible. So if we don’t make it, it is because of each individual. You can’t leave it to your politicians to represent you; you can’t leave it to your ministers to pray for you. It’s going to be how each individual reacts in relation to the truth.”
> 
> — Buckminster Fuller, 1978