+++
title = 'Trust'
date = 2022-11-26T17:21:35-05:00
draft = false
+++

![](./a1_1964_5_common-raven_doug_kliewer_kk.jpg "The Raven")

# Trust

Humans in their natural state of being are trusting creatures, they have to be to survive.
Humans are _pack_ animals, something akin to wolves.
Wolves, in their natural state, live in packs typically based on familial lines.
Many civilized humans, who are not in their natural state, often through applying their distorted perceptions, misconstrue the social structure of wolf packs.

While we humans can survive on our own, it is not likely that we can thrive by ourselves.
We simply are not built to be alone, we are tribal creatures.

Now the black bear, they are built different than we. 
While still a social creature, once they reach the age of _family breakup_, they can and do thrive by themselves. 

Then we have the ravens.
I do not claim to have great knowledge of their society, but I have spent time getting to know them, and they us.
The ravens have a complex society.

Their society also looks to be familial based. 
Ravens have an entirely dedicated manner of speech they only use with their clan.
Ravens are one of the people who pay a lot of attention, and interact with other species.

Ravens are so good at this, they can recognize humans by their face.
What's more, ravens remember the actions of an individual human.
In other words, ravens get to know people.

This means that the raven is going to establish a relationship with you.
They are going to watch and learn, and never forget. 
They are also going to tell their friends about you.
In other words, what you do and how you behave is going to establish your reputation with your local raven clans.

Do what you will, but we prefer a cordial relationship with our neighbors.
Most people in their natural state do prefer a peaceful, predictable, mutually beneficial relationship with their neighbors.
Healthy people prefer peace, hate is not a natural state of being.

We humans need peace, cooperation, belonging, and trust, at a minimum to thrive.
None of these can be achieved individually.
They are only achieved through relationship.
We need each other to thrive.

Once you have spent some time with ravens, you will establish a relationship.
Ravens are a very mistrusting people. 
For very good reasons.

But over time, you can build trust with raven. 
And they might even decide they like you and want to spend time with you.
Raven is very observant, you can learn a lot from them, and for the most part they seem to follow the golden rule.

'Treat others in the way you want to be treated.'

If individuals are healthy, and follow this rule, peace will usually follow.
Should you choose to hate, hate is what you will get back.

Ravens have learned to be mistrusting of their human neighbors.
Rightfully so, given the amount of hate humans throw at them.
The raven never forgets. 
They know you through your actions.
They don't know if you are ill, evil, ignorant, etc. nor do they care.

The raven observes, the raven remembers, the raven acts accordingly.
The raven does not need to _trust_ you, they _know_ you.
The raven does not deny their reality.
The raven does not judge.

We civilized humans are broken.
Who do we know?
Who do we trust?

Unlike the raven, it is in our nature to be trusting.
Yet, our society demonstrates time and time again, that it can not be trusted.
We wander through artificial spaces passing face after face who remain to us, unknown, untrusted.

I know raven, I trust raven.
I have come to know the humans that surround us all.
I most certainly do not trust them.

Given enough time, enough support, enough kindness, trust can come.
But ours is a ruthless society. 
Ruthlessness breeds hate. 
Hate breeds distrust.
Which leaves us weak and easy to control.

It is through peace in which we will find our communal strength.
To have peace, we are going to have to build trust.
To build trust, we are going to need time to treat one another kindly, to build trust through our actions not just our words.
(This is not to say words are not actions in themselves and are most certainly important.)

We are going to have to remember.
We need to establish relationship with one another and that relationship needs to be trusting, consenting, and beneficial.

To get there is going to take a lot of work.
The first step is to work on yourself.
Then help your chosen family.
Then together, help others.

We have a lot of work to do to bring strength through peace.
The alternative is hell on earth.
We decide which it will be through our deeds. 














