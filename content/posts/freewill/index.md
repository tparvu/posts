+++
title = 'Freewill'
date = 2024-06-08T17:21:35-05:00
draft = false
+++

![freewill](freewill.jpg)

# freewill

If we humans do indeed possess the ability to make choices, then we should understand why we are making the choices that we are. 
It might appear at first, that understanding the "why" of the thing you are doing is trivial.
It is not.

We all have varying degrees of self awareness, that waxes and wanes throughout our life.
We find ourselves sometimes making decisions, that seem right at the time, but in retrospect were obviously not the correct choice.

In many cases, we can see later, that we were indeed not in the _right frame of mind_ or we hadn't thought it through well, or maybe we were just tired and distracted.
Often we are reacting more so than making a choice.
This "reaction" is particularly fueled by emotions.

We have all been in some situation where had we been calmer, more focused, less heated, we would have made a choice different than that made in the heat of the moment.

But before we can make a choice, we have to figure out what it is that we want to be making a choice about.
We must spend time in self reflection to understand who we are and how we want to behave.

We can get ideas about what our morals could be from others, but it is our choice, our freewill, to decide how we wish to act.

One of the more common ethics, which has been stated in many forms, one of which is, "Do unto others as you would have them do unto you."

This is a simple, yet powerful ethical framework, and is the basis of morality in many religions.
It is simple enough, that a child can understand it.

> I don't want to be punched in the face, so I am not going to punch him in the face.
>
>I don't want her to steal my lunch money, so I will not steal hers.
>
>I don't like it when people make fun of me, so I shouldn't make fun of other people.
>
>It hurts me when people deceive me, so I will not deceive others.
>
>I like it when people treat me kindly, so I will be kind to others.

The "Golden Rule" can be thought of as a method of self realization through the exercise of freewill.
In practicing this ancient ethic, we foster empathy of others through understanding how we feel, and how that relates to how they feel.

Using just this simple ethical framework, we can rationally guide our decisions, as they relate to others.
In so doing, we can place a check on our selfish desires and wild emotions.

>Bobby was making fun of me, saying a lot of really mean things, and I wanted to punch him in the face.
>But I realized that two wrongs do not make a right, and I wouldn't want anyone to punch me, so I didn't punch him.

This is all child's play, but we learn who we are, or discover who we are, if you will, as we grow into adults.
Can any of this help in a more complex moral dilemma?

What about slavery?
Is being a slaver morally OK?
It sure is nice to have free labor and servants.
I treat my slaves pretty well, I mean what's the problem here?

Would you want to be a slave?
Would you want to be a well treated slave?
Would you trade places with your slaves?

Most people do not want to be enslaved.
To have no authority over their own lives.
To have no say in what they can or can not do.
So, how could it be morally justified to hold people in slavery?

Plenty of people throughout history have made their justifications for slavery, have made the choice to be slavers.
They justify their actions.
Slavers have a morality in which it is OK to force others to do what you would not want done to you.
That is a moral choice.
Often this choice is rationalized by superiority over others.
That is entitlement, over others, because somehow you are more deserving and better than the "subhumans".

Some people are racists, and they are perfectly OK with that.
Others, like myself believe that racism is immoral as is slavery.
So, who is right?

This is where you come in, you decide who is right and who is wrong.
That is your freewill at work.
You know who you are, and what you believe, so you decide who is right, and who is wrong.
You also decide what you want to do about that decision.
Do you want to oppose racists or do you want to support them?
Maybe neither, that is your decision, and you are the one who will live with it.
As will we all.

"The unexamined life is not worth living", someone once said.
Your gift, your song, is your freewill, your moral agency. 
For you to travel through life reacting as opposed to acting, would make you no different from any non-sentient being. 
You would be nothing more than cause and effect.

But if you take your agency, determine who you are, and make choices accordingly, well then you are much more than a cause or an effect, you are a force.

Simple, right? 
Yeah, not so much.

The ego is the separation of self, from everything else.
We often identify ourselves with our egos. 
This isn't a bad thing per se, but it does cause some problems.

Every parent knows the suffering of our children's suffering.
The crying at 3am, you haven't slept much at all in the past couple of days.
Your ego might tell you, this is crazy, you need to get some sleep, just ignore the crying, put in ear plugs, do something.
Your ego is playing your own inner crying baby. 

A parent overcomes their ego, their frustration, exhaustion, their own needs, in favor of their child's.
A parent knows they love their child.
They know they want the best for them, and will do anything to help them.
You rationally, and emotionally, get past your ego, through your love of your child.

This drama plays out in many of the choices we make.

>Do I tell this dude that he dropped a $20 or do I just pick it up?
>
>Nobody at work is going to know if I grab a drink and don't pay for it, what's the big deal?
>
>That elderly lady could really use some help loading her car, but I'm kinda busy right now.
>
>Crap, there goes that wrapper I dropped, blowing down the street, do I really need to go grab it?

The selfishness of the ego opposed against the loving kindness of the heart.

The ego serves a purpose, but it also gets in our way when we are not aware of it.

When we have spent time reflecting on who we are, we can use that knowledge to help guide us.

I know I love my children.
I know I want the best for them.
So, when I feel frustrated with them, I recognize this is my ego, not my heart. 
Then I can use my mind, my will, to rationally apply my work of self realization, to help me act in accordance with who I know I am.

Of course, this doesn't work all the time.
Sometimes you get angry, sometimes you react, perhaps yelling at your child.
However, when you do calm back down, and realize that you probably hurt your child by yelling at them, you go back and you make it right.
You explain to them, that you are sorry for yelling at them.
That you were emotional and upset in the moment, and now that you have had some time to reflect on your actions, you regret how you acted and you are sorry.
You can then learn from this, and hopefully in the future, you will act in ways you needn't apologize for.

Simple, right? ;^)

> Everyone has a song
> 
> God gave us each a song
> 
> That's how we know who we are
> 
> Everyone has a song

If you don't know who you are, then you will be ripe for manipulation.
Your agency usurped by some others will.
For many of us, the first part of our life, is discovering who we are.

Possessing freewill we naturally have the choice to willfully not become self aware.
To figuratively stick our fingers in our ears and ignore our own agency.
This choice however, like all others, comes with consequences.
My experiences have led me to believe that not being true to who you are, comes with some pretty negative consequences.
Often the pain is too much to bear, and we try to numb it with drugs, TV, adrenaline, thirst for power, and so on.

Experience has shown me that those who are true to themselves, who know who they are, move through life with passion, humility and grace.
There are also those, that in being true to themselves, move through life with ruthless devotion to themselves.
Each to their own. 

Then there are those who are being driven by the currents of circumstance. 
Reacting more so than acting.
Those with the will and the skills can manipulate these reactions into extensions of their own will.
Those who wish to serve the other, can try to aid these reactionary actors into becoming themselves, and thus becoming free in their agency.

No matter if you choose to serve self, others, or choose not to decide, you still have made a choice.
When you are still discovering who you are, I offer this piece of advice, that a San Francisco cop once told a homeless teen in Golden Gate Park.

"Do not do anything out here, that you can not live with later."

It is good to experiment, but if you ever make a choice that you can't live with, then it is too late. 
With freedom, comes responsibility and consequences. 
You don't want to have to learn to live with something you can't.

There are those that die without ever knowing themselves.
Living lives, driven by the currents of circumstance and others.
Just following "the plan" or what have you.

Some folks reach "middle age" and begin to realize they haven't been true to themselves.
Often they panic and react, buying some expensive toys, having affairs, slamming their Ducati into a flatbed truck at 130MPH.
Sometimes, though people dig deep, do the work, and begin their life's journey in earnest at last.

Some people never find the space to discover themselves.
Too busy surviving, running, making money.
Usually, this is by choice.
Some of us, long before we know anything about anything, are traumatized to the point where our real self withdraws deep inside of us, and sometimes never ventures forth again.

This is life.
This is freedom.
Freewill.

Some have all the privilege in the world.
Have the means to do anything they please.
Yet, they still choose not to know themselves.

It is your life, it is your choice, it is never too late until the end.























